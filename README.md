keepIt
======

Gestionnaire de check-lists simple propulsé par angularjs, angular-xeditable et bootstrap.

Le code de départ est celui du site officiel d'[AngularJS](https://angularjs.org/)

Fonctionnalités :

- check-lists
- notes au format markdown
- sauvegarde des données en local (localStorage)
- recherche par tag
- copie du contenu des notes et todos
- écran d'accueil avec listes et notes favorites

[Démo en ligne](https://keepit.5apps.com)
