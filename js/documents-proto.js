function Todo(title, tasks,tags) {
    this.title = title || "Todo";
    this.tasks = tasks || [];
    this.tags = tags || [];
    this.star = false;
    this.tags.sort();
}

Todo.prototype = {
    addTask: function(task) {
        if(task instanceof Task) {
            this.tasks.push(task);
        }
        else {
            console.debug("Pas une tache");
        }
    },
    delTask: function(taskNum) {
        if(taskNum<this.tasks.length) {
            this.tasks.splice(taskNum,1);
            console.debug("Effacement de la tache n°"+taskNum);
        }
        else {
            console.debug("Cette tache n'existe pas !");
        }
    },
    addTag: function(tag) {
        // empeche tags vides et doublons
        var lowTags = this.tags.join("~").toLowerCase().split("~");
        if(tag.length>0 && lowTags.indexOf(tag.toLowerCase())<0) {
            this.tags.push(tag);
            this.tags.sort();
        }
    },
    delTag: function(tag) {
        var lowTags = this.tags.join("~").toLowerCase().split("~"),
            index = lowTags.indexOf(tag.toLowerCase());
        if(index>=0) {
            this.tags.splice(index,1);
        } else {
             console.debug("Le tag \'"+tag+"\' n'existe pas ");
        }
    },
    toString: function() {
         var txt = this.title+"\n\n";
         for (var i=0, lg=this.tasks.length; i<lg; i++) {
             txt+= (this.tasks[i].done) ? "[x] ":"[ ] ";
             txt+=this.tasks[i].name+"\n";
         }
         if (this.tags.length>0) {
             txt+="\nTags: "+this.tags.join(", ");
         }
         return txt;
    }
};

function Task(name, done) {
    this.name = name || "";
    this.done = done || false;
}

function Note(title, content, creationDate, tags) {
    this.title = title || "Note";
    this.content = content || "";
    this.creationDate = creationDate || Date.now();
    this.tags = tags || [];
    this.star = false;
}

Note.prototype = {
    addTag: function(tag) {
        var lowTags = this.tags.join("~").toLowerCase().split("~");
        if(tag.length>0 && lowTags.indexOf(tag.toLowerCase())<0) {
            this.tags.push(tag);
            this.tags.sort();
        }
    },
    delTag: function(tag) {
        var lowTags = this.tags.join("~").toLowerCase().split("~"),
            index = lowTags.indexOf(tag.toLowerCase());
        if(index>=0) {
            this.tags.splice(index,1);
        } else {
             console.debug("Le tag \'"+tag+"\' n'existe pas ");
        }
    },
    toString: function() {
        var txt = this.title+"\n\n"+this.content;
        if (this.tags.length>0) {
            txt+="\n\nTags: "+this.tags.join(", ");
        }
        return txt;
    }
};

/* données pour test interface et fonctionnalités */
var myTodo1 = new Todo('Mon Todo 1',[], ["JS","JavaScript"]);
var myTodo2 = new Todo('Mon Todo 2',[], ["JS","JavaScript","Angular"]);
var noteContent = "#keepIt\n\nEdition de *check-lists* et de notes au format [markdown](https://fr.wikipedia.org/wiki/Markdown).\n\nFonctionnalités:\n";
noteContent +="- listes multiples\n- notes au format markdown\n- sauvegarde LocalStorage";
noteContent +="\n- copie du contenu notes et listes\n- favoris\n- recherche par tag";
var myNote = new Note("Ma note",noteContent,null, ["JS","JavaScript"]);
myTodo1.addTask(new Task("Tache 1", false));
myTodo1.addTask(new Task("Tache 2", true));
myTodo2.addTask(new Task("Tache 1", true));
myTodo2.addTask(new Task("Tache 2", false));
myTodo2.addTask(new Task("Tache 3", true));
myTodo1.star = true;
myTodo2.star = true;
myNote.star = true;
console.log(myTodo1.toString());
console.log(myTodo2.toString());
console.log(myNote.toString());
