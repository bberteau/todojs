angular.module('keepIt')
.factory('Todos', function() {
    var todos = {};
    todos.getAll = function() {
        var storage = JSON.parse(localStorage.getItem('ki-todos'));
        if (!storage) {
            return [myTodo1, myTodo2];
        }
        var todosList = [];
        for (var i = 0, lg = storage.length; i < lg; i++) {
            var entry = storage[i];
            var todo = new Todo(entry.title);
            todo.star = entry.star;
            for (var j = 0, tagsNum = entry.tags.length; j < tagsNum; j++) {
                todo.addTag(entry.tags[j]);
            }
            for (var j = 0, tasksNum = entry.tasks.length; j < tasksNum; j++) {
                todo.addTask(new Task(entry.tasks[j].name, entry.tasks[j].done));
            }
            todosList.push(todo);
        }
        return todosList;
    };
    todos.save = function(data) {
        localStorage.setItem('ki-todos', JSON.stringify(data));
    };
    todos.getFav = function() {
        var allTodos = todos.getAll();
        var todosList = [];
        for (var i = 0, lg = allTodos.length; i < lg; i++) {
            var todo = allTodos[i];
            if (todo.star) {
                todo.index = i;
                todosList.push(todo);
            }
        }
        return todosList;
    };
    todos.getTagged = function(tag) {
        var allTodos = todos.getAll();
        var todosList = [];
        for(var i = 0, lg = allTodos.length; i < lg; i++) {
            var todo = allTodos[i];
            lowTags = todo.tags.join("~").toLowerCase().split("~");
            if (lowTags.indexOf(tag.toLowerCase())>=0) {
                todo.index = i;
                todosList.push(todo);
            }
        }
        return todosList;
    };
    todos.save = function(data) {
        localStorage.setItem('ki-todos', JSON.stringify(data));
    };
    return todos;
});
