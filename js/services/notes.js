angular.module('keepIt')
.factory('Notes', function() {
    notes = {};
    notes.getAll = function() {
        var storage = JSON.parse(localStorage.getItem('ki-notes'));
        var notesList = [];
        if (!storage) {
            return [myNote];
        }
        for (var i = 0, lg = storage.length; i < lg; i++) {
            var entry = storage[i];
            var note = new Note(entry.title, entry.content);
            note.star = entry.star;
            for (var j = 0, tagsNum = entry.tags.length; j < tagsNum; j++) {
                note.addTag(entry.tags[j]);
            }
            notesList.push(note);
        }
        return notesList;
    };
    notes.getFav = function() {
        var allNotes = notes.getAll();
        var notesList = [];
        for (var i = 0, lg = allNotes.length; i < lg; i++) {
            var note = allNotes[i];
            if (note.star) {
                note.index = i;
                notesList.push(note);
            }
        }
        return notesList;
    };
    notes.getTagged = function(tag) {
        var allNotes = notes.getAll();
        var notesList = [];
        for (var i = 0, lg = allNotes.length; i < lg; i++) {
            var note = allNotes[i];
            var lowTags = note.tags.join("~").toLowerCase().split("~");
            if (lowTags.indexOf(tag.toLowerCase())>=0) {
                note.index = i;
                notesList.push(note);
            }
        }
        return notesList;
    };
    notes.save = function(data) {
        localStorage.setItem('ki-notes', JSON.stringify(data));
    };
    return notes;
});
