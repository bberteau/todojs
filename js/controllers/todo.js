angular.module('keepIt')
.controller('TodoEditController', ['$scope','$routeParams', '$location','Todos', function($scope, $routeParams, $location, Todos) {
    // var todoEdit = this;
    var todoId = $routeParams.todoId;
    $scope.todos = Todos.getAll();
    $scope.todo = $scope.todos[todoId];
    if(((parseFloat(todoId) != parseInt(todoId)) || isNaN(todoId)) || todoId >= $scope.todos.length) {
        console.debug("La liste "+todoId+" n'existe pas !");
        $location.path('/todos');
        $location.replace();
    }

    // Nombre de taches restantes
    $scope.remaining = function() {
      var count = 0;
      angular.forEach($scope.todo.tasks, function(task) {
        count += task.done ? 0 : 1;
      });
      return count;
    };

    // Suppression des taches faites
    $scope.archive = function() {
        var oldTasks = $scope.todo.tasks;
        $scope.todo.tasks = [];
        angular.forEach(oldTasks, function(task) {
            if (!task.done) {
                $scope.todo.tasks.push(task);
            }
      });
      Todos.save($scope.todos);
    };

    // Efface le stockage local
    $scope.clearLocalStorage = function() {
        localStorage.removeItem('ki-todos');
        $scope.todo = [];
    };

    // Sauve les données dans le stockage local
    $scope.saveLocalStorage = function(data) {
        Todos.save($scope.todos);
    };

    // Ajout d'un tache
    $scope.addTask = function() {
      if ($scope.todoText.trim().length>0) {
            $scope.todo.addTask(new Task($scope.todoText, false));
            $scope.todoText = '';
            dataString = JSON.stringify($scope.todos);
            Todos.save($scope.todos);
        }
    };

    // Efface tache
    $scope.delTask = function(index) {
        $scope.todo.delTask(index);
        Todos.save($scope.todos);
    };

    // Suppression de la liste
    $scope.delTodo = function() {
        $scope.todos.splice(todoId,1);
        $location.path('/todos');
        $location.replace();
        Todos.save($scope.todos);
    };

    // Ajout de tags
    $scope.addTag = function(tag) {
        if ($scope.tag.trim().length>0) {
            $scope.todo.addTag($scope.tag);
            $scope.tag = "";
        }
        Todos.save($scope.todos);
    };

    // Suppression de tags
    $scope.delTag = function(tag) {
        if (tag) {
            $scope.todo.delTag(tag);
        }
        Todos.save($scope.todos);
    };

    // Importation de données dans le todo - Pas implémenté
    $scope.importData = function() {
        console.log("Importation des données");
    };

    $scope.toggleStar = function() {
        $scope.todo.star = !$scope.todo.star;
        Todos.save($scope.todos);
    };
  }]);

angular.module('keepIt')
.controller('TodosListController', ['$scope','$location', 'Todos', function($scope, $location, Todos) {
    $scope.todos = Todos.getAll();

    // Suppression d'une liste
    $scope.delTodo = function(id) {
        console.debug("Efface todo n°"+id);
        $scope.todos.splice(id,1);
        Todos.save($scope.todos);
    };

    // Création d'un nouveau todo
    $scope.addTodo = function() {
        $scope.todos.push(new Todo());
        Todos.save($scope.todos);
        $scope.editTodo($scope.todos.length-1);
    };

    // Edition d'un todo
    $scope.editTodo = function(todoId) {
        $location.path('/todo/'+(todoId));
        $location.replace();
    };

    $scope.toggleStar = function(todo) {
        todo.star = !todo.star;
        Todos.save($scope.todos);
    };

    $scope.changeLocation = function(newPath) {
        $location.path(newPath);
        $location.replace();
    };
}]);
