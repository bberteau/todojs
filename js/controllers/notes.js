angular.module('keepIt')
.controller('NotesListController', ['$scope','$routeParams', '$location','Notes', function($scope, $routeParams, $location, Notes) {
    $scope.notes = Notes.getAll();

    // Activiation / désactivation étoile
    $scope.toggleStar = function(note) {
        note.star = !note.star;
        Notes.save($scope.notes);
    };

    // Création d'un nouveau todo
    $scope.addNote = function() {
        $scope.notes.push(new Note());
        Notes.save($scope.notes);
        $location.path('/note/'+($scope.notes.length-1));
        $location.replace();
    };

    // Suppression d'une note
    $scope.delNote = function(id) {
        $scope.notes.splice(id,1);
        Notes.save($scope.notes);
    };

    // Edition d'une note
    $scope.editNote = function(id) {
        $location.path('/note/'+id);
        $location.replace();
    };

    $scope.changeLocation = function(newPath) {
        $location.path(newPath);
        $location.replace();
    };

  }]);

angular.module('keepIt')
.controller('NoteEditController', ['$scope','$routeParams', '$location','Notes', function($scope, $routeParams, $location, Notes) {
    var noteId = $routeParams.noteId;
    if(((parseFloat(noteId) != parseInt(noteId)) || isNaN(noteId)) || noteId >= Notes.getAll().length) {
        console.debug("La note "+noteId+" n'existe pas !");
        $location.path('/notes');
        $location.replace();
    }

    $scope.notes = Notes.getAll();
    $scope.note = $scope.notes[noteId];
    // Suppression d'une note
    $scope.delNote = function() {
        notes.splice(noteId,1);
        $location.path('/notes');
        $location.replace();
        Notes.save($scope.notes);
    };

    // Ajout de tags
    $scope.addTag = function(tag) {
    if ($scope.tag.trim().length>0) {
        $scope.note.addTag($scope.tag);
            $scope.tag = "";
        }
        Notes.save($scope.notes);
    };

    // Suppression de tags
    $scope.delTag = function(tag) {
        if (tag) {
            $scope.note.delTag(tag);
        }
        Notes.save($scope.notes);
    };

    // Activation / désactivation étoile
    $scope.toggleStar = function() {
        $scope.note.star = !$scope.note.star;
        Notes.save($scope.notes);
    };

    // Sauve les données dans le stockage local
    $scope.saveLocalStorage = function(data) {
        Notes.save($scope.notes);
    };


}]);

