angular.module('keepIt')
.controller('findController', ['$scope', '$window', '$routeParams', '$location','Notes', 'Todos', function($scope, $window, $routeParams, $location, Notes, Todos) {
    $scope.todos = Todos.getAll();
    $scope.notes = Notes.getAll();
    $scope.changeLocation = function(newPath) {
        $location.path(newPath);
        $location.replace();
    };
    if ($location.path()==="/home") {
        // Accueil application : Favoris
        $scope.pageTitle = "Bienvenue dans keepIt";
        $scope.noResults = "Pas de favoris";
        $scope.todosFound = Todos.getFav();
        $scope.notesFound = Notes.getFav();
    } else {
        // Filtrage par tag
        $scope.tag = $routeParams.tag;
        $scope.pageTitle = "#"+$scope.tag;
        $scope.noResults = "Aucun document avec le tag #"+$scope.tag;
        $scope.todosFound = Todos.getTagged($scope.tag);
        $scope.notesFound = Notes.getTagged($scope.tag);
    }
    $scope.toggleStar = function(doc) {
        doc.star = !doc.star;
        if (doc instanceof Todo) {
            $scope.todos[doc.index].star = !$scope.todos[doc.index].star;
            Todos.save($scope.todos);
        } else {
            $scope.notes[doc.index].star = !$scope.notes[doc.index].star;
            Notes.save($scope.notes);
        }
        if ($location.path()==="/home") {
            $window.location.reload();
        }
    };
}]);

