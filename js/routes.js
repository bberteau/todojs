angular.module('keepIt')
.config(function($routeProvider){
    $routeProvider.when('/todo/:todoId', {
        templateUrl: 'templates/todo-edit.html',
        controller: 'TodoEditController',
        controllerAs: 'todoEdit'
    })
    .when('/todos', {
        templateUrl: 'templates/todos.html',
        controller: 'TodosListController',
        controllerAs: 'todosList'
    })
    .when('/notes', {
        templateUrl: 'templates/notes.html',
        controller: 'NotesListController',
        controllerAs: 'notesList'
    })
    .when('/note/:noteId', {
        templateUrl: 'templates/note-edit.html',
        controller: 'NoteEditController',
        controllerAs: 'noteEdit'
    })
    .when('/aide', {
        templateUrl: 'templates/aide.html'
    })
    .when('/tag/:tag',{
        templateUrl: 'templates/find.html',
        controller: 'findController',
        controllerAs: 'findCtrl'
    })
    .when('/home',{
        templateUrl: 'templates/find.html',
        controller: 'findController',
        controllerAs: 'findCtrl'
    })
    .otherwise( { redirectTo: '/home' } );
});

